#!/bin/bash
#
# Copyright Alexander Sosna <alexander@sosna.de>
#

# Update
apt-get update
apt-get full-upgrade

# Install basic tools
apt-get install -y mosh wget curl w3m silversearcher-ag tmux terminator vim neovim htop tree git tig lm-sensors apt-file memtest86+ etckeeper ncdu rsync zstd nmap zsh lf

# Install drivers, hardware tools
apt-get install -y firmware-linux-nonfree firmware-misc-nonfree
apt-get install -y firmware-amd-graphics 
apt-get install -y radeontop
apt-get install -y lm-sensors smartmontools

# Container and virtualisation
apt-get install -y docker.io
apt-get install -y virt-manager 

# Publish website
apt-get install -y sshpass hugo
#!/bin/sh
apt-get update
apt-get full-upgrade
