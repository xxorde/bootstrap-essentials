#!/bin/bash
#
# Copyright Alexander Sosna <alexander@sosna.de>
#

# Configure apt to use contrib and non-free
apt-add-repository -y contrib
apt-add-repository -y non-free

# Update
apt-get update

# Install basic tools
apt-get install -y mosh wget curl w3m silversearcher-ag tmux vim neovim htop tree git tig etckeeper ncdu rsync zstd nmap zsh lf

# Install drivers, hardware tools
apt-get install -y firmware-linux-nonfree firmware-misc-nonfree
apt-get install -y lm-sensors smartmontools

# Sway environment
apt-get install -y sway foot

# Ham radio essentials
apt-get install -y grig gnuradio wsjtx js8call jtdx putty fldigi pat

# OpenWebRX
wget -O - https://repo.openwebrx.de/debian/key.gpg.txt | gpg --dearmor -o /usr/share/keyrings/openwebrx.gpg
echo "deb [signed-by=/usr/share/keyrings/openwebrx.gpg] https://repo.openwebrx.de/debian/ bullseye main" > /etc/apt/sources.list.d/openwebrx.list
apt-get update
apt-get install -y openwebrx
