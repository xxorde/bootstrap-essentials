#!/bin/bash
#
# Copyright Alexander Sosna <alexander@sosna.de>
#

# Configure apt to use contrib and non-free
apt-add-repository -y contrib
apt-add-repository -y non-free

# Update
apt-get update

# Install basic tools
apt-get install -y mosh wget curl w3m silversearcher-ag tmux terminator vim neovim htop tree git tig etckeeper ncdu rsync zstd nmap zsh lf

# Install drivers, hardware tools
apt-get install -y firmware-linux-nonfree firmware-misc-nonfree
apt-get install -y firmware-amd-graphics libgl1-mesa-dri libglx-mesa0 mesa-vulkan-drivers xserver-xorg-video-all
apt-get install -y radeontop
apt-get install -y lm-sensors smartmontools

# Sway environment
apt-get install -y sway foot

# Container and virtualisation
apt-get install -y docker.io
apt-get install -y virt-manager 

# Image tools
apt-get install -y imagemagick
apt-get install -y gimp

# Install main applications
apt-get install -y thunderbird thunderbird-l10n-de vlc gnumeric 
apt-get install -y syncthing syncthing-gtk #syncthingtray

# Wine
apt-get install -y wine

# Ham radio essentials
apt-get install -y qrq grig gnuradio wsjtx js8call jtdx putty fldigi pat chirp

## Ham logging
apt-get install -y pyqso klog tucnak

## Gridtracker
echo "deb [signed-by=/usr/share/keyrings/gridtracker.asc]  http://debian.gridtracker.org/debian/ ./" > /etc/apt/sources.list.d/gridtracker.list
curl --output - http://debian.gridtracker.org/gridtracker-debrepo.gpg | sudo tee /usr/share/keyrings/gridtracker.asc
apt-get update
sudo apt-get install -y gridtracker

# Publish website
apt-get install -y sshpass hugo

# Rust
apt-get install -y rust-all # full set
apt-get install -y cargo rustc # small set 

# Chess
apt-get install -y eboard
apt-get install -y stockfish 
apt-get install -y gnuchess gnuchess-book

# VSCodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
apt-get update
apt-get install -y codium

# 3D Printing
apt-get install -y cura openscad meshlab
